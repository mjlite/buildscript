#!/bin/bash

#TODO
#
#fix texStudio
#post install kdenlive2020
#post install enve
#post install blender


################################################################################################
# Setup all working folders and download iso
################################################################################################
machine=${uname -m}
USER_HOME=$(getent passwd $SUDO_USER | cut -d: -f6)
mkdir ${USER_HOME}/dev/buildScript/livecdtmp
cd ${USER_HOME}/dev/buildScript/livecdtmp

apt install squashfs-tools genisoimage resolvconf

if [ "$1" = "32" ]; then
    if [ "$2" = "18" ]; then
        xubuntu="xubuntu-18.04.4-desktop-i386.iso"
        release="18.04.4"
    elif [ "$2" = "20" ]; then
        xubuntu="xubuntu-20.04-desktop-i386.iso"
        release="20.04"
    else
        xubuntu="xubuntu-18.04.4-desktop-i386.iso"
        release="18.04.4"
    fi
else
    if [ "$2" = "18" ]; then
        xubuntu="xubuntu-18.04.4-desktop-amd64.iso"
        release="18.04.4"
    elif [ "$2" = "20" ]; then
        xubuntu="xubuntu-20.04-desktop-amd64.iso"
        release="20.04"
    else
        xubuntu="xubuntu-18.04.4-desktop-amd64.iso"
        release="18.04.4"
    fi
fi

if [ -f "$xubuntu" ]; then
    echo "File $xubuntu already exists, skipping download"
else
    echo "File $xubuntu does not exist downloading"
    read -p "Press any key to download"
    if [ "$1" = "32" ]; then
        wget http://mirror.csclub.uwaterloo.ca/xubuntu-releases/$release/release/$xubuntu
    else
        wget http://mirror.csclub.uwaterloo.ca/xubuntu-releases/$release/release/$xubuntu
    fi
fi

mkdir extract-cd mnt

mount -o loop $xubuntu mnt
rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract-cd
unsquashfs mnt/casper/filesystem.squashfs
mv squashfs-root edit
cp /etc/resolv.conf edit/etc/
mount --bind /dev/ edit/dev

read -p "Press any key to continue"

################################################################################################
# This puts us into a chroot sandbox
################################################################################################
cat << EOF | sudo chroot edit

################################################################################################
# Mount necessary devices
################################################################################################
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C

################################################################################################
# Add custom ppa's and repositories
################################################################################################

echo "Adding repositories"

echo "Mesa drivers"
echo "deb http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list
echo "# deb-src http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list
echo "deb http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list.save

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F63F0F2B90935439

#add-apt-repository ppa:cwchien/gradle -y
#add-apt-repository ppa:kisak/kisak-mesa -y
#add-apt-repository ppa:tista/adapta -y
#add-apt-repository ppa:papirus/papirus -y
#add-apt-repository ppa:numix/ppa -y
apt update

################################################################################################
# Remove installed packages and stuff I dont want
################################################################################################

echo "Removing packages"
apt remove -y --purge --autoremove gimp libreoffice* sgt-puzzles sgt-launcher gnome-mines gnome-games gnome-sudoku snap snapd vlc pidgin parole unattended-upgrades xubuntu-* xfce4-*

################################################################################################
# Install packages
################################################################################################

#echo "Installing packages"
#apt install -y gufw gdebi mpv geany geany-plugins hardinfo keepassxc shotwell htop compton gnome-disk-utility fcitx-mozc bleachbit adapta-gtk-theme papirus-icon-theme numix-gtk-theme numix-icon-theme-circle
#apt install blender gradle git abiword gnumeric
#apt upgrade

################################################################################################
# Change partition manager, remount on errors
################################################################################################

echo "Fixing ext3 libPartman"
sed -i 's/errors=remount/noatime,nodiratime,errors=remount/g' /lib/partman/fstab.d/ext3

################################################################################################
# Cleaning up
################################################################################################
#rm files

apt-get clean
apt-get autoremove -y
rm -rf /tmp/* ~/.bash_history
umount /proc
umount /sys
umount /dev/pts

################################################################################################
# This drops us out of the chroot
################################################################################################
EOF

echo "Unmounting development mounts"
umount edit/dev
umount mnt

read -p "Press any key to continue building ISO"

echo "Removing old filesystem"
rm ${USER_HOME}/dev/buildScript/livecdtmp/extract-cd/casper/filesystem.squashfs

echo "Rebuilding squashFS"
cd ${USER_HOME}/dev/buildScript/livecdtmp
mksquashfs edit extract-cd/casper/filesystem.squashfs
nano extract-cd/README.diskdefines

echo "Rebuilding md5"
cd extract-cd
rm md5sum.txt
find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt

echo "Generating ISO file"
cd ${USER_HOME}/dev/buildScript/livecdtmp
mkisofs -r -V "REMASTER" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ./cleanISO.iso extract-cd
chmod 777 ./cleanISO.iso

echo "Building complete"

