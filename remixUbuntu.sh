#!/bin/bash

#TODO
#
#fix texStudio
#post install kdenlive2020
#post install enve
#post install blender


################################################################################################
# Setup all working folders and download iso
################################################################################################
machine=${uname -m}
USER_HOME=$(getent passwd $SUDO_USER | cut -d: -f6)
mkdir ${USER_HOME}/livecdtmp
cd ${USER_HOME}/livecdtmp

apt install squashfs-tools genisoimage resolvconf

if [ "$1" = "32" ]; then
    if [ "$2" = "18" ]; then
        xubuntu="xubuntu-18.04.4-desktop-i386.iso"
        release="18.04.4"
    elif [ "$2" = "20" ]; then
        xubuntu="xubuntu-20.04-desktop-i386.iso"
        release="20.04"
    else
        xubuntu="xubuntu-18.04.4-desktop-i386.iso"
        release="18.04.4"
    fi
else
    if [ "$2" = "18" ]; then
        xubuntu="xubuntu-18.04.4-desktop-amd64.iso"
        release="18.04.4"
    elif [ "$2" = "20" ]; then
        xubuntu="xubuntu-20.04-desktop-amd64.iso"
        release="20.04"
    else
        xubuntu="xubuntu-18.04.4-desktop-amd64.iso"
        release="18.04.4"
    fi
fi

if [ -f "$xubuntu" ]; then
    echo "File $xubuntu already exists, skipping download"
else
    echo "File $xubuntu does not exist downloading"
    read -p "Press any key to download"
    if [ "$1" = "32" ]; then
        wget http://mirror.csclub.uwaterloo.ca/xubuntu-releases/$release/release/$xubuntu
    else
        wget http://mirror.csclub.uwaterloo.ca/xubuntu-releases/$release/release/$xubuntu
    fi
fi

mkdir extract-cd mnt

mount -o loop $xubuntu mnt
rsync --exclude=/casper/filesystem.squashfs -a mnt/ extract-cd
unsquashfs mnt/casper/filesystem.squashfs
mv squashfs-root edit
cp /etc/resolv.conf edit/etc/
mount --bind /dev/ edit/dev

read -p "Press any key to continue"

################################################################################################
# Generate some needed files for later
################################################################################################

echo "Generating installation profile for texLive2020"
touch ./installation.profile

echo "# It will NOT be updated and reflects only the" >> ./installation.profile
echo "# texlive.profile written on Fri Jul 10 04:25:27 2020 UTC" >> ./installation.profile
echo "# installation profile at installation time." >> ./installation.profile
echo "selected_scheme scheme-basic" >> ./installation.profile
echo "TEXDIR /usr/local/texlive/2020" >> ./installation.profile
echo "TEXMFCONFIG ${USER_HOME}/.texlive2020/texmf-config" >> ./installation.profile
echo "TEXMFHOME ${USER_HOME}/texmf" >> ./installation.profile
echo "TEXMFLOCAL /usr/local/texlive/texmf-local" >> ./installation.profile
echo "TEXMFSYSCONFIG /usr/local/texlive/2020/texmf-config" >> ./installation.profile
echo "TEXMFSYSVAR /usr/local/texlive/2020/texmf-var" >> ./installation.profile
echo "TEXMFVAR ${USER_HOME}/.texlive2020/texmf-var" >> ./installation.profile
echo "binary_x86_64-linux 1" >> ./installation.profile
echo "instopt_adjustpath 0" >> ./installation.profile
echo "instopt_adjustrepo 1" >> ./installation.profile
echo "instopt_letter 0" >> ./installation.profile
echo "instopt_portable 0" >> ./installation.profile
echo "instopt_write18_restricted 1" >> ./installation.profile
echo "tlpdbopt_autobackup 1" >> ./installation.profile
echo "tlpdbopt_backupdir tlpkg/backups" >> ./installation.profile
echo "tlpdbopt_create_formats 1" >> ./installation.profile
echo "tlpdbopt_desktop_integration 1" >> ./installation.profile
echo "tlpdbopt_file_assocs 1" >> ./installation.profile
echo "tlpdbopt_generate_updmap 0" >> ./installation.profile
echo "tlpdbopt_install_docfiles 1" >> ./installation.profile
echo "tlpdbopt_install_srcfiles 1" >> ./installation.profile
echo "tlpdbopt_post_code 1" >> ./installation.profile
echo "tlpdbopt_sys_bin /usr/local/bin" >> ./installation.profile
echo "tlpdbopt_sys_info /usr/local/share/info" >> ./installation.profile
echo "tlpdbopt_sys_man /usr/local/share/man" >> ./installation.profile
echo "tlpdbopt_w32_multi_user 1" >> ./installation.profile
mv ./installation.profile ${USER_HOME}/livecdtmp/edit/opt/

################################################################################################
# xfce settings config files
################################################################################################
echo "Generating xfce4 config folder structure"
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/terminal
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel

echo "Generating xfce4-desktop.xml"
touch ./xfce4-desktop.xml

echo '<?xml version="1.0" encoding="UTF-8"?>' >> ./xfce4-desktop.xml
echo '' >> ./xfce4-desktop.xml
echo '<channel name="xfce4-desktop" version="1.0">' >> ./xfce4-desktop.xml
echo '  <property name="desktop-icons" type="empty">' >> ./xfce4-desktop.xml
echo '    <property name="style" type="empty"/>' >> ./xfce4-desktop.xml
echo '    <property name="file-icons" type="empty">' >> ./xfce4-desktop.xml
echo '      <property name="show-home" type="empty"/>' >> ./xfce4-desktop.xml
echo '      <property name="show-filesystem" type="empty"/>' >> ./xfce4-desktop.xml
echo '      <property name="show-removable" type="empty"/>' >> ./xfce4-desktop.xml
echo '      <property name="show-trash" type="empty"/>' >> ./xfce4-desktop.xml
echo '    </property>' >> ./xfce4-desktop.xml
echo '    <property name="icon-size" type="empty"/>' >> ./xfce4-desktop.xml
echo '    <property name="tooltip-size" type="empty"/>' >> ./xfce4-desktop.xml
echo '  </property>' >> ./xfce4-desktop.xml
echo '  <property name="backdrop" type="empty">' >> ./xfce4-desktop.xml
echo '    <property name="screen0" type="empty">' >> ./xfce4-desktop.xml
echo '      <property name="monitor0" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="image-path" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-style" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-show" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '      <property name="monitor1" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="image-path" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-style" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-show" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '      <property name="monitor2" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="image-path" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-style" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-show" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '      <property name="monitor3" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="image-path" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-style" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="image-show" type="empty"/>' >> ./xfce4-desktop.xml
echo '        <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '      <property name="monitorVGA-1" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="workspace0" type="empty">' >> ./xfce4-desktop.xml
echo '          <property name="color-style" type="int" value="0"/>' >> ./xfce4-desktop.xml
echo '          <property name="image-style" type="int" value="5"/>' >> ./xfce4-desktop.xml
echo '          <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '        </property>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '      <property name="monitoreDP-1" type="empty">' >> ./xfce4-desktop.xml
echo '        <property name="workspace0" type="empty">' >> ./xfce4-desktop.xml
echo '          <property name="color-style" type="int" value="0"/>' >> ./xfce4-desktop.xml
echo '          <property name="image-style" type="int" value="5"/>' >> ./xfce4-desktop.xml
echo '          <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '        </property>' >> ./xfce4-desktop.xml
echo '        <property name="workspace1" type="empty">' >> ./xfce4-desktop.xml
echo '          <property name="color-style" type="int" value="0"/>' >> ./xfce4-desktop.xml
echo '          <property name="image-style" type="int" value="5"/>' >> ./xfce4-desktop.xml
echo '          <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '        </property>' >> ./xfce4-desktop.xml
echo '        <property name="workspace2" type="empty">' >> ./xfce4-desktop.xml
echo '          <property name="color-style" type="int" value="0"/>' >> ./xfce4-desktop.xml
echo '          <property name="image-style" type="int" value="5"/>' >> ./xfce4-desktop.xml
echo '          <property name="last-image" type="string" value="/usr/share/xfce4/backdrops/61077.jpg"/>' >> ./xfce4-desktop.xml
echo '        </property>' >> ./xfce4-desktop.xml
echo '      </property>' >> ./xfce4-desktop.xml
echo '    </property>' >> ./xfce4-desktop.xml
echo '  </property>' >> ./xfce4-desktop.xml
echo '  <property name="last" type="empty">' >> ./xfce4-desktop.xml
echo '    <property name="window-width" type="int" value="728"/>' >> ./xfce4-desktop.xml
echo '    <property name="window-height" type="int" value="667"/>' >> ./xfce4-desktop.xml
echo '  </property>' >> ./xfce4-desktop.xml
echo '</channel>' >> ./xfce4-desktop.xml

cp ./xfce4-desktop.xml ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/

echo "Generating xfce4-panel.xml"
touch ./xfce4-panel.xml

echo '<?xml version="1.0" encoding="UTF-8"?>' >> ./xfce4-panel.xml
echo '' >> ./xfce4-panel.xml
echo '<channel name="xfce4-panel" version="1.0">' >> ./xfce4-panel.xml
echo '  <property name="panels" type="uint" value="1">' >> ./xfce4-panel.xml
echo '    <property name="panel-0" type="empty">' >> ./xfce4-panel.xml
echo '      <property name="position" type="string" value="p=6;x=0;y=0"/>' >> ./xfce4-panel.xml
echo '      <property name="length" type="uint" value="100"/>' >> ./xfce4-panel.xml
echo '      <property name="position-locked" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="plugin-ids" type="array">' >> ./xfce4-panel.xml
echo '        <value type="int" value="1"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="2"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="16"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="17"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="18"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="3"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="4"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="13"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="14"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="5"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="6"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="7"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="8"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="9"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="10"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="11"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="12"/>' >> ./xfce4-panel.xml
echo '        <value type="int" value="15"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '      <property name="background-style" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="size" type="uint" value="32"/>' >> ./xfce4-panel.xml
echo '      <property name="length-adjust" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="span-monitors" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '      <property name="mode" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="autohide-behavior" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '  </property>' >> ./xfce4-panel.xml
echo '  <property name="plugins" type="empty">' >> ./xfce4-panel.xml
echo '    <property name="plugin-1" type="string" value="whiskermenu"/>' >> ./xfce4-panel.xml
echo '    <property name="plugin-2" type="string" value="separator">' >> ./xfce4-panel.xml
echo '      <property name="style" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="expand" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-3" type="string" value="tasklist">' >> ./xfce4-panel.xml
echo '      <property name="show-handle" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '      <property name="flat-buttons" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-4" type="string" value="separator">' >> ./xfce4-panel.xml
echo '      <property name="style" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="expand" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-5" type="string" value="systray">' >> ./xfce4-panel.xml
echo '      <property name="show-frame" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '      <property name="square-icons" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="size-max" type="uint" value="22"/>' >> ./xfce4-panel.xml
echo '      <property name="names-ordered" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="thunar"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-6" type="string" value="notification-plugin"/>' >> ./xfce4-panel.xml
echo '    <property name="plugin-7" type="string" value="indicator">' >> ./xfce4-panel.xml
echo '      <property name="blacklist" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="libappmenu.so"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '      <property name="square-icons" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="known-indicators" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="com.canonical.indicator.messages"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-8" type="string" value="statusnotifier">' >> ./xfce4-panel.xml
echo '      <property name="icon-size" type="uint" value="22"/>' >> ./xfce4-panel.xml
echo '      <property name="menu-is-primary" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="mode-whitelist" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '      <property name="single-row" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '      <property name="square-icons" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="symbolic-icons" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="known-items" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="nm-applet"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-9" type="string" value="power-manager-plugin"/>' >> ./xfce4-panel.xml
echo '    <property name="plugin-10" type="string" value="pulseaudio">' >> ./xfce4-panel.xml
echo '      <property name="enable-keyboard-shortcuts" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="enable-mpris" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '      <property name="mixer-command" type="string" value="pavucontrol"/>' >> ./xfce4-panel.xml
echo '      <property name="mpris-players" type="string" value="parole"/>' >> ./xfce4-panel.xml
echo '      <property name="show-notifications" type="bool" value="true"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-11" type="string" value="separator">' >> ./xfce4-panel.xml
echo '      <property name="style" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="expand" type="bool" value="false"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-12" type="string" value="clock">' >> ./xfce4-panel.xml
echo '      <property name="digital-format" type="string" value="%d %b, %H:%M"/>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-13" type="string" value="pager"/>' >> ./xfce4-panel.xml
echo '    <property name="plugin-14" type="string" value="weather"/>' >> ./xfce4-panel.xml
echo '    <property name="plugin-15" type="string" value="actions">' >> ./xfce4-panel.xml
echo '      <property name="appearance" type="uint" value="0"/>' >> ./xfce4-panel.xml
echo '      <property name="items" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="-lock-screen"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-switch-user"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-separator"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-suspend"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-hibernate"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-hybrid-sleep"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-separator"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-shutdown"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-restart"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-separator"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="+logout"/>' >> ./xfce4-panel.xml
echo '        <value type="string" value="-logout-dialog"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-16" type="string" value="launcher">' >> ./xfce4-panel.xml
echo '      <property name="items" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="15947009221.desktop"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-17" type="string" value="launcher">' >> ./xfce4-panel.xml
echo '      <property name="items" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="15947009392.desktop"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '    <property name="plugin-18" type="string" value="launcher">' >> ./xfce4-panel.xml
echo '      <property name="items" type="array">' >> ./xfce4-panel.xml
echo '        <value type="string" value="15947009503.desktop"/>' >> ./xfce4-panel.xml
echo '      </property>' >> ./xfce4-panel.xml
echo '    </property>' >> ./xfce4-panel.xml
echo '  </property>' >> ./xfce4-panel.xml
echo '  <property name="configver" type="int" value="2"/>' >> ./xfce4-panel.xml
echo '</channel>' >> ./xfce4-panel.xml

cp ./xfce4-panel.xml ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/

echo "Generating xsettings.xml"
touch ./xsettings.xml

echo '<?xml version="1.0" encoding="UTF-8"?>' >> ./xsettings.xml
echo '' >> ./xsettings.xml
echo '<channel name="xsettings" version="1.0">' >> ./xsettings.xml
echo '  <property name="Net" type="empty">' >> ./xsettings.xml
echo '    <property name="ThemeName" type="string" value="Adapta-Nokto"/>' >> ./xsettings.xml
echo '    <property name="IconThemeName" type="string" value="Numix-Circle"/>' >> ./xsettings.xml
echo '    <property name="DoubleClickTime" type="empty"/>' >> ./xsettings.xml
echo '    <property name="DoubleClickDistance" type="empty"/>' >> ./xsettings.xml
echo '    <property name="DndDragThreshold" type="empty"/>' >> ./xsettings.xml
echo '    <property name="CursorBlink" type="empty"/>' >> ./xsettings.xml
echo '    <property name="CursorBlinkTime" type="empty"/>' >> ./xsettings.xml
echo '    <property name="SoundThemeName" type="empty"/>' >> ./xsettings.xml
echo '    <property name="EnableEventSounds" type="empty"/>' >> ./xsettings.xml
echo '    <property name="EnableInputFeedbackSounds" type="empty"/>' >> ./xsettings.xml
echo '    <property name="FallbackIconTheme" type="empty"/>' >> ./xsettings.xml
echo '  </property>' >> ./xsettings.xml
echo '  <property name="Xft" type="empty">' >> ./xsettings.xml
echo '    <property name="DPI" type="int" value="96"/>' >> ./xsettings.xml
echo '    <property name="Antialias" type="int" value="1"/>' >> ./xsettings.xml
echo '    <property name="Hinting" type="empty"/>' >> ./xsettings.xml
echo '    <property name="HintStyle" type="string" value="hintfull"/>' >> ./xsettings.xml
echo '    <property name="RGBA" type="empty"/>' >> ./xsettings.xml
echo '    <property name="Lcdfilter" type="empty"/>' >> ./xsettings.xml
echo '  </property>' >> ./xsettings.xml
echo '  <property name="Gtk" type="empty">' >> ./xsettings.xml
echo '    <property name="CanChangeAccels" type="empty"/>' >> ./xsettings.xml
echo '    <property name="ColorPalette" type="empty"/>' >> ./xsettings.xml
echo '    <property name="FontName" type="empty"/>' >> ./xsettings.xml
echo '    <property name="MonospaceFontName" type="empty"/>' >> ./xsettings.xml
echo '    <property name="IconSizes" type="empty"/>' >> ./xsettings.xml
echo '    <property name="KeyThemeName" type="empty"/>' >> ./xsettings.xml
echo '    <property name="ToolbarStyle" type="empty"/>' >> ./xsettings.xml
echo '    <property name="ToolbarIconSize" type="empty"/>' >> ./xsettings.xml
echo '    <property name="MenuImages" type="empty"/>' >> ./xsettings.xml
echo '    <property name="ButtonImages" type="empty"/>' >> ./xsettings.xml
echo '    <property name="MenuBarAccel" type="empty"/>' >> ./xsettings.xml
echo '    <property name="CursorThemeName" type="empty"/>' >> ./xsettings.xml
echo '    <property name="CursorThemeSize" type="empty"/>' >> ./xsettings.xml
echo '    <property name="DecorationLayout" type="empty"/>' >> ./xsettings.xml
echo '  </property>' >> ./xsettings.xml
echo '</channel>' >> ./xsettings.xml

cp ./xsettings.xml ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/

echo "Generating terminalrc"
touch ./terminalrc

echo '[Configuration]' >> ./terminalrc
echo 'BackgroundMode=TERMINAL_BACKGROUND_TRANSPARENT' >> ./terminalrc
echo 'ColorForeground=#b7b7b7' >> ./terminalrc
echo 'ColorBackground=#131926' >> ./terminalrc
echo 'ColorCursor=#0f4999' >> ./terminalrc
echo 'ColorSelection=#163b59' >> ./terminalrc
echo 'ColorSelectionUseDefault=FALSE' >> ./terminalrc
echo 'ColorBold=#ffffff' >> ./terminalrc
echo 'ColorBoldUseDefault=FALSE' >> ./terminalrc
echo 'ColorPalette=#000000;#aa0000;#44aa44;#aa5500;#0039aa;#aa22aa;#1a92aa;#aaaaaa;#777777;#ff8787;#4ce64c;#ded82c;#295fcc;#cc58cc;#4ccce6;#ffffff' >> ./terminalrc
echo 'FontName=DejaVu Sans Mono 11' >> ./terminalrc
echo 'MiscAlwaysShowTabs=FALSE' >> ./terminalrc
echo 'MiscBell=FALSE' >> ./terminalrc
echo 'MiscBellUrgent=FALSE' >> ./terminalrc
echo 'MiscBordersDefault=TRUE' >> ./terminalrc
echo 'MiscCursorBlinks=FALSE' >> ./terminalrc
echo 'MiscCursorShape=TERMINAL_CURSOR_SHAPE_BLOCK' >> ./terminalrc
echo 'MiscDefaultGeometry=80x24' >> ./terminalrc
echo 'MiscInheritGeometry=FALSE' >> ./terminalrc
echo 'MiscMenubarDefault=TRUE' >> ./terminalrc
echo 'MiscMouseAutohide=FALSE' >> ./terminalrc
echo 'MiscMouseWheelZoom=TRUE' >> ./terminalrc
echo 'MiscToolbarDefault=FALSE' >> ./terminalrc
echo 'MiscConfirmClose=TRUE' >> ./terminalrc
echo 'MiscCycleTabs=TRUE' >> ./terminalrc
echo 'MiscTabCloseButtons=TRUE' >> ./terminalrc
echo 'MiscTabCloseMiddleClick=TRUE' >> ./terminalrc
echo 'MiscTabPosition=GTK_POS_TOP' >> ./terminalrc
echo 'MiscHighlightUrls=TRUE' >> ./terminalrc
echo 'MiscMiddleClickOpensUri=FALSE' >> ./terminalrc
echo 'MiscCopyOnSelect=FALSE' >> ./terminalrc
echo 'MiscShowRelaunchDialog=TRUE' >> ./terminalrc
echo 'MiscRewrapOnResize=TRUE' >> ./terminalrc
echo 'MiscUseShiftArrowsToScroll=FALSE' >> ./terminalrc
echo 'MiscSlimTabs=FALSE' >> ./terminalrc
echo 'MiscNewTabAdjacent=FALSE' >> ./terminalrc
echo 'TabActivityColor=#0f4999' >> ./terminalrc
echo 'BackgroundDarkness=0.750000' >> ./terminalrc

cp ./terminalrc ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/terminal/

echo "Generating whiskermenu-1.rc"
touch ./whiskermenu-1.rc

echo 'favorites=exo-web-browser.desktop,exo-mail-reader.desktop,exo-file-manager.desktop,org.gnome.Software.desktop,exo-terminal-emulator.desktop,xfhelp4.desktop' >> ./whiskermenu-1.rc
echo 'recent=geany.desktop' >> ./whiskermenu-1.rc
echo 'button-title=Applications Menu' >> ./whiskermenu-1.rc
echo 'button-icon=xubuntu-logo-menu' >> ./whiskermenu-1.rc
echo 'button-single-row=false' >> ./whiskermenu-1.rc
echo 'show-button-title=false' >> ./whiskermenu-1.rc
echo 'show-button-icon=true' >> ./whiskermenu-1.rc
echo 'launcher-show-name=true' >> ./whiskermenu-1.rc
echo 'launcher-show-description=false' >> ./whiskermenu-1.rc
echo 'launcher-show-tooltip=true' >> ./whiskermenu-1.rc
echo 'item-icon-size=3' >> ./whiskermenu-1.rc
echo 'hover-switch-category=false' >> ./whiskermenu-1.rc
echo 'category-show-name=true' >> ./whiskermenu-1.rc
echo 'category-icon-size=2' >> ./whiskermenu-1.rc
echo 'load-hierarchy=true' >> ./whiskermenu-1.rc
echo 'recent-items-max=10' >> ./whiskermenu-1.rc
echo 'favorites-in-recent=true' >> ./whiskermenu-1.rc
echo 'display-recent-default=false' >> ./whiskermenu-1.rc
echo 'position-search-alternate=true' >> ./whiskermenu-1.rc
echo 'position-commands-alternate=false' >> ./whiskermenu-1.rc
echo 'position-categories-alternate=false' >> ./whiskermenu-1.rc
echo 'menu-width=433' >> ./whiskermenu-1.rc
echo 'menu-height=662' >> ./whiskermenu-1.rc
echo 'menu-opacity=100' >> ./whiskermenu-1.rc
echo 'command-settings=xfce4-settings-manager' >> ./whiskermenu-1.rc
echo 'show-command-settings=true' >> ./whiskermenu-1.rc
echo 'command-lockscreen=xflock4' >> ./whiskermenu-1.rc
echo 'show-command-lockscreen=true' >> ./whiskermenu-1.rc
echo 'command-switchuser=dm-tool switch-to-greeter' >> ./whiskermenu-1.rc
echo 'show-command-switchuser=false' >> ./whiskermenu-1.rc
echo 'command-logout=xfce4-session-logout' >> ./whiskermenu-1.rc
echo 'show-command-logout=true' >> ./whiskermenu-1.rc
echo 'command-menueditor=menulibre' >> ./whiskermenu-1.rc
echo 'show-command-menueditor=true' >> ./whiskermenu-1.rc
echo 'command-profile=mugshot' >> ./whiskermenu-1.rc
echo 'show-command-profile=true' >> ./whiskermenu-1.rc
echo 'search-actions=5' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc
echo '[action0]' >> ./whiskermenu-1.rc
echo 'name=Man Pages' >> ./whiskermenu-1.rc
echo 'pattern=#' >> ./whiskermenu-1.rc
echo 'command=exo-open --launch TerminalEmulator man %s' >> ./whiskermenu-1.rc
echo 'regex=false' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc
echo '[action1]' >> ./whiskermenu-1.rc
echo 'name=Web Search' >> ./whiskermenu-1.rc
echo 'pattern=?' >> ./whiskermenu-1.rc
echo 'command=exo-open --launch WebBrowser https://duckduckgo.com/?q=%u' >> ./whiskermenu-1.rc
echo 'regex=false' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc
echo '[action2]' >> ./whiskermenu-1.rc
echo 'name=Wikipedia' >> ./whiskermenu-1.rc
echo 'pattern=!w' >> ./whiskermenu-1.rc
echo 'command=exo-open --launch WebBrowser https://en.wikipedia.org/wiki/%u' >> ./whiskermenu-1.rc
echo 'regex=false' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc
echo '[action3]' >> ./whiskermenu-1.rc
echo 'name=Run in Terminal' >> ./whiskermenu-1.rc
echo 'pattern=!' >> ./whiskermenu-1.rc
echo 'command=exo-open --launch TerminalEmulator %s' >> ./whiskermenu-1.rc
echo 'regex=false' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc
echo '[action4]' >> ./whiskermenu-1.rc
echo 'name=Open URI' >> ./whiskermenu-1.rc
echo 'pattern=^(file|http|https):\\/\\/(.*)$' >> ./whiskermenu-1.rc
echo 'command=exo-open \\0' >> ./whiskermenu-1.rc
echo 'regex=true' >> ./whiskermenu-1.rc
echo '' >> ./whiskermenu-1.rc

cp ./whiskermenu-1.rc ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/

echo "Generating weather-14.rc"
touch ./weather-14.rc

echo 'loc_name=Richmond, Metro Vancouver Regional District' >> ./weather-14.rc
echo 'lat=49.163168' >> ./weather-14.rc
echo 'lon=-123.137414' >> ./weather-14.rc
echo 'msl=5' >> ./weather-14.rc
echo 'timezone=America/Vancouver' >> ./weather-14.rc
echo 'offset=-07:00' >> ./weather-14.rc
echo 'cache_file_max_age=172800' >> ./weather-14.rc
echo 'power_saving=true' >> ./weather-14.rc
echo 'units_temperature=0' >> ./weather-14.rc
echo 'units_pressure=0' >> ./weather-14.rc
echo 'units_windspeed=0' >> ./weather-14.rc
echo 'units_precipitation=0' >> ./weather-14.rc
echo 'units_altitude=0' >> ./weather-14.rc
echo 'model_apparent_temperature=0' >> ./weather-14.rc
echo 'round=true' >> ./weather-14.rc
echo 'single_row=true' >> ./weather-14.rc
echo 'tooltip_style=1' >> ./weather-14.rc
echo 'forecast_layout=1' >> ./weather-14.rc
echo 'forecast_days=5' >> ./weather-14.rc
echo 'scrollbox_animate=true' >> ./weather-14.rc
echo 'theme_dir=/usr/share/xfce4/weather/icons/liquid' >> ./weather-14.rc
echo 'show_scrollbox=true' >> ./weather-14.rc
echo 'scrollbox_lines=1' >> ./weather-14.rc
echo 'scrollbox_color=#000000000000' >> ./weather-14.rc
echo 'scrollbox_use_color=false' >> ./weather-14.rc
echo 'label0=3' >> ./weather-14.rc
echo 'label1=7' >> ./weather-14.rc
echo 'label2=5' >> ./weather-14.rc

cp ./weather-14.rc ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/

echo "Generating panel folders"
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-16
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-17
mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-18

echo "Generating shortcuts for launcher 16"
touch ./15947009221.desktop

echo '[Desktop Entry]' >> ./15947009221.desktop
echo 'Version=1.0' >> ./15947009221.desktop
echo 'Name=Firefox Web Browser' >> ./15947009221.desktop
echo 'Comment=Browse the World Wide Web' >> ./15947009221.desktop
echo 'GenericName=Web Browser' >> ./15947009221.desktop
echo 'Keywords=Internet;WWW;Browser;Web;Explorer' >> ./15947009221.desktop
echo 'Exec=firefox %u' >> ./15947009221.desktop
echo 'Terminal=false' >> ./15947009221.desktop
echo 'X-MultipleArgs=false' >> ./15947009221.desktop
echo 'Type=Application' >> ./15947009221.desktop
echo 'Icon=firefox' >> ./15947009221.desktop
echo 'Categories=GNOME;GTK;Network;WebBrowser;' >> ./15947009221.desktop
echo 'MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;x-scheme-handler/ftp;x-scheme-handler/chrome;video/webm;application/x-xpinstall;' >> ./15947009221.desktop
echo 'StartupNotify=true' >> ./15947009221.desktop
echo 'Actions=new-window;new-private-window;' >> ./15947009221.desktop
echo 'X-XFCE-Source=file:///usr/share/applications/firefox.desktop' >> ./15947009221.desktop
echo '' >> ./15947009221.desktop
echo '[Desktop Action new-window]' >> ./15947009221.desktop
echo 'Name=Open a New Window' >> ./15947009221.desktop
echo 'Exec=firefox -new-window' >> ./15947009221.desktop
echo '' >> ./15947009221.desktop
echo '[Desktop Action new-private-window]' >> ./15947009221.desktop
echo 'Name=Open a New Private Window' >> ./15947009221.desktop
echo 'Exec=firefox -private-window' >> ./15947009221.desktop
echo '' >> ./15947009221.desktop

cp ./15947009221.desktop ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-16/

echo "Generating shortcuts for launcher 17"
touch ./15947009392.desktop

echo '[Desktop Entry]' >> ./15947009392.desktop
echo 'Version=1.0' >> ./15947009392.desktop
echo 'Type=Application' >> ./15947009392.desktop
echo 'Exec=exo-open --launch FileManager %u' >> ./15947009392.desktop
echo 'Icon=system-file-manager' >> ./15947009392.desktop
echo 'StartupNotify=true' >> ./15947009392.desktop
echo 'Terminal=false' >> ./15947009392.desktop
echo 'Categories=Utility;X-XFCE;X-Xfce-Toplevel;' >> ./15947009392.desktop
echo 'OnlyShowIn=XFCE;' >> ./15947009392.desktop
echo 'X-XFCE-MimeType=inode/directory;x-scheme-handler/trash;' >> ./15947009392.desktop
echo 'Name=File Manager' >> ./15947009392.desktop
echo 'Comment=Browse the file system' >> ./15947009392.desktop
echo 'X-XFCE-Source=file:///usr/share/applications/exo-file-manager.desktop' >> ./15947009392.desktop
echo '' >> ./15947009392.desktop

cp ./15947009392.desktop ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-17/

echo "Generating shortcuts for launcher 18"
touch ./15947009503.desktop

echo '[Desktop Entry]' >> ./15947009503.desktop
echo 'Version=1.0' >> ./15947009503.desktop
echo 'Type=Application' >> ./15947009503.desktop
echo 'Exec=exo-open --launch TerminalEmulator' >> ./15947009503.desktop
echo 'Icon=utilities-terminal' >> ./15947009503.desktop
echo 'StartupNotify=true' >> ./15947009503.desktop
echo 'Terminal=false' >> ./15947009503.desktop
echo 'Categories=Utility;X-XFCE;X-Xfce-Toplevel;' >> ./15947009503.desktop
echo 'OnlyShowIn=XFCE;' >> ./15947009503.desktop
echo 'Name=Terminal Emulator' >> ./15947009503.desktop
echo 'Comment=Use the command line' >> ./15947009503.desktop
echo 'X-XFCE-Source=file:///usr/share/applications/exo-terminal-emulator.desktop' >> ./15947009503.desktop
echo '' >> ./15947009503.desktop

cp ./15947009503.desktop ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/panel/launcher-18/

echo "Generating autostart for compton"
touch ./Compton.desktop

echo '[Desktop Entry]' >> ./Compton.desktop
echo 'Encoding=UTF-8' >> ./Compton.desktop
echo 'Version=0.9.4' >> ./Compton.desktop
echo 'Type=Application' >> ./Compton.desktop
echo 'Name=Compton' >> ./Compton.desktop
echo 'Comment=fix xfce4 tearing' >> ./Compton.desktop
echo 'Exec=compton --vsync opengl-swc --backend glx' >> ./Compton.desktop
echo 'OnlyShowIn=XFCE;' >> ./Compton.desktop
echo 'StartupNotify=false' >> ./Compton.desktop
echo 'Terminal=false' >> ./Compton.desktop
echo 'Hidden=false' >> ./Compton.desktop
echo '' >> ./Compton.desktop

mkdir -p ${USER_HOME}/livecdtmp/edit/etc/skel/.config/autostart
cp ./Compton.desktop ${USER_HOME}/livecdtmp/edit/etc/skel/.config/autostart/

echo "Generating autostart for no repeat keys (for sensitive keyboards)"
touch ./NoKeyRepeat.desktop

echo '[Desktop Entry]' >> ./NoKeyRepeat.desktop
echo 'Encoding=UTF-8' >> ./NoKeyRepeat.desktop
echo 'Version=0.9.4' >> ./NoKeyRepeat.desktop
echo 'Type=Application' >> ./NoKeyRepeat.desktop
echo 'Name=NoKeyRepeat' >> ./NoKeyRepeat.desktop
echo 'Comment=Disable repeat keys' >> ./NoKeyRepeat.desktop
echo 'Exec=xset r off' >> ./NoKeyRepeat.desktop
echo 'OnlyShowIn=XFCE;' >> ./NoKeyRepeat.desktop
echo 'StartupNotify=true' >> ./NoKeyRepeat.desktop
echo 'Terminal=true' >> ./NoKeyRepeat.desktop
echo 'Hidden=true' >> ./NoKeyRepeat.desktop
echo '' >> ./NoKeyRepeat.desktop

cp ./NoKeyRepeat.desktop ${USER_HOME}/livecdtmp/edit/etc/skel/.config/autostart/

echo "Generating xfwm4.xml"
touch ./xfwm4.xml

echo '<?xml version="1.0" encoding="UTF-8"?>' >> ./xfwm4.xml
echo '' >> ./xfwm4.xml
echo '<channel name="xfwm4" version="1.0">' >> ./xfwm4.xml
echo '  <property name="general" type="empty">' >> ./xfwm4.xml
echo '    <property name="activate_action" type="string" value="bring"/>' >> ./xfwm4.xml
echo '    <property name="borderless_maximize" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="box_move" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="box_resize" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="button_layout" type="string" value="|HMC"/>' >> ./xfwm4.xml
echo '    <property name="button_offset" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="button_spacing" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="click_to_focus" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="cycle_apps_only" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="cycle_draw_frame" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="cycle_hidden" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="cycle_minimum" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="cycle_preview" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="cycle_tabwin_mode" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="cycle_workspaces" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="double_click_action" type="string" value="maximize"/>' >> ./xfwm4.xml
echo '    <property name="double_click_distance" type="int" value="5"/>' >> ./xfwm4.xml
echo '    <property name="double_click_time" type="int" value="250"/>' >> ./xfwm4.xml
echo '    <property name="easy_click" type="string" value="Alt"/>' >> ./xfwm4.xml
echo '    <property name="focus_delay" type="int" value="250"/>' >> ./xfwm4.xml
echo '    <property name="focus_hint" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="focus_new" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="frame_opacity" type="int" value="100"/>' >> ./xfwm4.xml
echo '    <property name="full_width_title" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="horiz_scroll_opacity" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="inactive_opacity" type="int" value="100"/>' >> ./xfwm4.xml
echo '    <property name="maximized_offset" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="mousewheel_rollup" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="move_opacity" type="int" value="100"/>' >> ./xfwm4.xml
echo '    <property name="placement_mode" type="string" value="center"/>' >> ./xfwm4.xml
echo '    <property name="placement_ratio" type="int" value="20"/>' >> ./xfwm4.xml
echo '    <property name="popup_opacity" type="int" value="100"/>' >> ./xfwm4.xml
echo '    <property name="prevent_focus_stealing" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="raise_delay" type="int" value="250"/>' >> ./xfwm4.xml
echo '    <property name="raise_on_click" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="raise_on_focus" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="raise_with_any_button" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="repeat_urgent_blink" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="resize_opacity" type="int" value="100"/>' >> ./xfwm4.xml
echo '    <property name="scroll_workspaces" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="shadow_delta_height" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="shadow_delta_width" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="shadow_delta_x" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="shadow_delta_y" type="int" value="-3"/>' >> ./xfwm4.xml
echo '    <property name="shadow_opacity" type="int" value="50"/>' >> ./xfwm4.xml
echo '    <property name="show_app_icon" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="show_dock_shadow" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="show_frame_shadow" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="show_popup_shadow" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="snap_resist" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="snap_to_border" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="snap_to_windows" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="snap_width" type="int" value="10"/>' >> ./xfwm4.xml
echo '    <property name="sync_to_vblank" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="theme" type="string" value="Adapta-Nokto"/>' >> ./xfwm4.xml
echo '    <property name="tile_on_move" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="title_alignment" type="string" value="center"/>' >> ./xfwm4.xml
echo '    <property name="title_font" type="string" value="Sans Bold 9"/>' >> ./xfwm4.xml
echo '    <property name="title_horizontal_offset" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="titleless_maximize" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="title_shadow_active" type="string" value="false"/>' >> ./xfwm4.xml
echo '    <property name="title_shadow_inactive" type="string" value="false"/>' >> ./xfwm4.xml
echo '    <property name="title_vertical_offset_active" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="title_vertical_offset_inactive" type="int" value="0"/>' >> ./xfwm4.xml
echo '    <property name="toggle_workspaces" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="unredirect_overlays" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="urgent_blink" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="use_compositing" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="workspace_count" type="int" value="2"/>' >> ./xfwm4.xml
echo '    <property name="wrap_cycle" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="workspace_names" type="array">' >> ./xfwm4.xml
echo '      <value type="string" value="Workspace 1"/>' >> ./xfwm4.xml
echo '      <value type="string" value="Workspace 2"/>' >> ./xfwm4.xml
echo '    </property>' >> ./xfwm4.xml
echo '    <property name="wrap_layout" type="bool" value="true"/>' >> ./xfwm4.xml
echo '    <property name="wrap_resistance" type="int" value="10"/>' >> ./xfwm4.xml
echo '    <property name="wrap_windows" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="wrap_workspaces" type="bool" value="false"/>' >> ./xfwm4.xml
echo '    <property name="zoom_desktop" type="bool" value="true"/>' >> ./xfwm4.xml
echo '  </property>' >> ./xfwm4.xml
echo '</channel>' >> ./xfwm4.xml
echo '' >> ./xfwm4.xml

cp ./xfwm4.xml ${USER_HOME}/livecdtmp/edit/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/

################################################################################################
# Update path in .profile to include texLive
################################################################################################

echo "Updating profile path"
echo ' ' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo '# set PATH to include texLive if it exists' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo 'if [ -d "/usr/local/texlive/2020/bin/x86_64-linux" ] ; then' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo '    PATH="/usr/local/texlive/2020/bin/x86_64-linux:$PATH"' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo '    MANPATH="/usr/local/texlive/2020/texmf-dist/doc/man:$MANPATH"' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo '    INFOPATH="/usr/local/texlive/2020/texmf-dist/doc/info:$INFOPATH"' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo 'fi' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile
echo ' ' >> ${USER_HOME}/livecdtmp/edit/etc/skel/.profile

#setup shader for compton
echo 'Adding shader for grayscale'
touch ${USER_HOME}/livecdtmp/edit/etc/skel/.grayscale.glsl
echo 'uniform float opacity;' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo 'uniform bool inver_color;' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo 'sampler2D tex;' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo 'void main() {' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '    vec4 c = texture2D(tex, gl_TexCoord[0].xy);' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '    float y = dot(c.rgb, vec3(0.299, 0.587, 0x114));' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '    gl_FragColor = vec4(y, y, y, 1.0);' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '}' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl
echo '' >> ${USER_HOME}/livecdtmp/etc/skel/.grayscale.glsl

#fix bug associated with this compton issue
echo 'Fixing compton associated bug'
touch ${USER_HOME}/livecdtmp/usr/share/X11/xorg.conf.d/20-intel.conf
echo 'Section "Device"' >> ${USER_HOME}/livecdtmp/usr/share/X11/xorg.conf.d/20-intel.conf
echo '    Identifier "Intel Graphics"' >> ${USER_HOME}/livecdtmp/usr/share/X11/xorg.conf.d/20-intel.conf
echo '    Driver "intel"' >> ${USER_HOME}/livecdtmp/usr/share/X11/xorg.conf.d/20-intel.conf
echo 'EndSection' >> ${USER_HOME}/livecdtmp/usr/share/X11/xorg.conf.d/20-intel.conf

################################################################################################
# This puts us into a chroot sandbox
################################################################################################
cat << EOF | sudo chroot edit

################################################################################################
# Mount necessary devices
################################################################################################
mount -t proc none /proc
mount -t sysfs none /sys
mount -t devpts none /dev/pts
export HOME=/root
export LC_ALL=C

################################################################################################
# Make a spooky skeleton
################################################################################################
cd /etc/skel
mkdir apps dev libs Desktop Documents Downloads Music Pictures Public Templates Videos
cd /etc/skel/.config

echo "This will update apt, upgrade packages, uninstall programs not wanted, and install new packages"

################################################################################################
# Get dark theme for Geany and copy into skel
################################################################################################

echo "Getting Geany themes"
mkdir -p /etc/skel/.config/geany/colorschemes/
cd /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/bespin.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/black.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/darcula.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/dark.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/dark-colors.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/dark-fruit-salad.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/delt-dark.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/epsilon.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/fluffy.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/gedit.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/github.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/himbeere.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/inkpot.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/mc.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/metallic-bottle.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/monokai.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/notepad-plus-plus.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/oblivion2.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/pygments.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/railcasts2.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/retro.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/sleepy-pastel.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/slushpoppies.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/solarized-dark.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/solarized-light.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/spyder-dark.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/steampunk.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/tango-dark.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/tango-light.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/tinge.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/ubuntu.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/vibrant-ink.conf -P /etc/skel/.config/geany/colorschemes/
wget https://raw.github.com/geany/geany-themes/master/colorschemes/zenburn.conf -P /etc/skel/.config/geany/colorschemes/

################################################################################################
# Add custom ppa's and repositories
################################################################################################

echo "Adding repositories"

echo "Gradle builds"
echo "deb http://ppa.launchpad.net/cwchien/gradle/ubuntu bionic main" >> /etc/apt/sources.list.d/cwchien-ubuntu-gradle-bionic.list
echo "# deb-src http://ppa.launchpad.net/cwchien/gradle/ubuntu bionic main" >> /etc/apt/sources.list.d/cwchien-ubuntu-gradle-bionic.list
echo "deb http://ppa.launchpad.net/cwchien/gradle/ubuntu bionic main" >> /etc/apt/sources.list.d/cwchien-ubuntu-gradle-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/cwchien/gradle/ubuntu bionic main" >> /etc/apt/sources.list.d/cwchien-ubuntu-gradle-bionic.list.save

echo "Mesa drivers"
echo "deb http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list
echo "# deb-src http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list
echo "deb http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/kisak/kisak-mesa/ubuntu bionic main" >> /etc/apt/sources.list.d/kisak-mesa-bionic.list.save


echo "Adapta Themes"
echo "deb http://ppa.launchpad.net/tista/adapta/ubuntu bionic main" >> /etc/apt/sources.list.d/tista-adapta-theme-bionic.list
echo "# deb-src http://ppa.launchpad.net/tista/adapta/ubuntu bionic main" >> /etc/apt/sources.list.d/tista-adapta-theme-bionic.list
echo "deb http://ppa.launchpad.net/tista/adapta/ubuntu bionic main" >> /etc/apt/sources.list.d/tista-adapta-theme-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/tista/adapta/ubuntu bionic main" >> /etc/apt/sources.list.d/tista-adapta-theme-bionic.list.save


echo "Papirus Icons"
echo "deb http://ppa.launchpad.net/papirus/papirus/ubuntu bionic main" >> /etc/apt/sources.list.d/papirus-papirus-icons-bionic.list
echo "# deb-src http://ppa.launchpad.net/papirus/papirus/ubuntu bionic main" >> /etc/apt/sources.list.d/papirus-papirus-icons-bionic.list
echo "deb http://ppa.launchpad.net/papirus/papirus/ubuntu bionic main" >> /etc/apt/sources.list.d/papirus-papirus-icons-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/papirus/papirus/ubuntu bionic main" >> /etc/apt/sources.list.d/papirus-papirus-icons-bionic.list.save


echo "Numix Icons"
echo "deb http://ppa.launchpad.net/numix/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/numix-icons-bionic.list
echo "# deb-src http://ppa.launchpad.net/numix/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/numix-icons-bionic.list
echo "deb http://ppa.launchpad.net/numix/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/numix-icons-bionic.list.save
echo "# deb-src http://ppa.launchpad.net/numix/ppa/ubuntu bionic main" >> /etc/apt/sources.list.d/numix-icons-bionic.list.save

apt-key adv --keyserver keyserver.ubuntu.com --recv-keys D7CC6F019D06AF36
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F63F0F2B90935439
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 52B709720F164EEB
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E58A9D36647CAE7F
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys B76E53652D87398A

#add-apt-repository ppa:cwchien/gradle -y
#add-apt-repository ppa:kisak/kisak-mesa -y
#add-apt-repository ppa:tista/adapta -y
#add-apt-repository ppa:papirus/papirus -y
#add-apt-repository ppa:numix/ppa -y
apt update

################################################################################################
# Remove installed packages and stuff I dont want
################################################################################################

echo "Removing packages"
apt remove -y --purge gimp libreoffice* sgt-puzzles sgt-launcher gnome-mines gnome-games gnome-sudoku snap snapd vlc pidgin parole

################################################################################################
# Install packages
################################################################################################

echo "Installing packages"
apt install -y gufw gdebi mpv geany geany-plugins hardinfo keepassxc shotwell htop compton gnome-disk-utility fcitx-mozc bleachbit adapta-gtk-theme papirus-icon-theme numix-gtk-theme numix-icon-theme-circle
#apt install blender gradle git abiword gnumeric
#apt upgrade

################################################################################################
# Enable firewall
################################################################################################

echo "Enabling GUFW"
sed -i 's/=no/=yes/g' /etc/ufw/ufw.conf

################################################################################################
# Change partition manager, remount on errors
################################################################################################

echo "Fixing ext3 libPartman"
sed -i 's/errors=remount/noatime,nodiratime,errors=remount/g' /lib/partman/fstab.d/ext3

################################################################################################
# Deb files
################################################################################################

echo "Installing local debs"
cd /opt/

#echo "Downloading deb files for $machine"
#if [ $machine = "x86_64" ]; then
    wget https://launchpad.net/veracrypt/trunk/1.24-update4/+download/veracrypt-1.24-Update4-Ubuntu-18.04-amd64.deb -P /opt/
    #wget https://github.com/Zettlr/Zettlr/releases/download/v1.7.1/Zettlr-1.7.1-amd64.deb -P /opt/
    wget https://mega.nz/linux/MEGAsync/xUbuntu_18.04/amd64/megasync_4.3.3-4.1_amd64.deb -P /opt/
    wget https://mega.nz/linux/MEGAsync/xUbuntu_18.04/amd64/thunar-megasync_3.6.6+2.1_amd64.deb -P /opt/

    echo "Installing with GDebi"
    gdebi -q -n veracrypt-1.24-Update4-Ubuntu-18.04-amd64.deb
    #gdebi -q -n Zettlr-1.7.1-amd64.deb
    gdebi -q -n megasync_4.3.3-4.1_amd64.deb
    gdebi -q -n thunar-megasync_3.6.6+2.1_amd64.deb

    rm veracrypt-1.24-Update4-Ubuntu-18.04-amd64.deb
    #rm Zettlr-1.7.1-amd64.deb
    rm megasync_4.3.3-4.1_amd64.deb
    rm thunar-megasync_3.6.6+2.1_amd64.deb
#else
#    echo "Not yet implemented"
#fi

################################################################################################
# Get some media
################################################################################################

echo "Downloading wallpapers"
wget http://getwallpapers.com/wallpaper/full/c/e/f/61064.jpg -P /usr/share/xfce4/backdrops/
wget http://getwallpapers.com/wallpaper/full/3/1/e/61077.jpg -P /usr/share/xfce4/backdrops/
wget http://getwallpapers.com/wallpaper/full/a/f/7/61127.jpg -P /usr/share/xfce4/backdrops/

################################################################################################
# Get some files
################################################################################################

echo "Downloading libs"
wget http://downloads.sourceforge.net/irrlicht/irrlicht-1.8.4.zip -P /etc/skel/libs/
wget https://build.lwjgl.org/nightly/lwjgl.zip -P /etc/skel/libs/
wget https://sourceforge.net/projects/java-game-lib/files/Official%20Releases/LWJGL%202.9.3/lwjgl-2.9.3.zip -P /etc/skel/libs/
wget http://slick.ninjacave.com/slick.zip -P /etc/skel/libs/
wget https://github.com/mini2Dx/mini2Dx-project-generator/releases/latest/download/mini2Dx-project-generator-linux.zip -P /etc/skel/libs/

################################################################################################
# Get unetbootin
################################################################################################
wget https://github.com/unetbootin/unetbootin/releases/download/681/unetbootin-linux-681.bin -P /usr/bin/
wget https://github.com/unetbootin/unetbootin/releases/download/681/unetbootin-linux64-681.bin -P /usr/bin/
chmod +x /usr/bin/unetbootin-linux-681.bin
chmod +x /usr/bin/unetbootin-linux64-681.bin

################################################################################################
# TexLive2020
################################################################################################

echo "Installing texLive"

echo "Downloading source"
cd /opt/
wget http://mirror.ctan.org/systems/texlive/tlnet/install-tl-unx.tar.gz

#if [ -d "/opt/install-tl-unx.tar.gz" ] ; then
#    echo "Extracting tar"
#    mkdir -p /opt/installTL2020
#    tar xzvf 'install-tl-unx.tar.gz' -C /opt/installTL2020 --strip-components=1
#    cd installTL2020
#    mv /opt/installation.profile /opt/installTL2020/
#    echo "Installing texlive2020, may need user input"
#    sh /opt/installTL2020/install-tl
#    rm /opt/'install-tl-unx.tar.gz'
#    rm -rf /opt/installTL2020
#fi

################################################################################################
# Grayscale compton script
################################################################################################
echo "Generating compton grayscale script"
cd /etc/skel
echo 'compton --backend glx --glx-fshader-win "$(cat .grayscale.glsl)" >> grayscale.sh


################################################################################################
# Cleaning up
################################################################################################
#rm files

apt-get clean
apt-get autoremove -y
rm -rf /tmp/* ~/.bash_history
umount /proc
umount /sys
umount /dev/pts

################################################################################################
# This drops us out of the chroot
################################################################################################
EOF

echo "Unmounting development mounts"
umount edit/dev
umount mnt

read -p "Press any key to continue building ISO"

echo "Removing old filesystem"
rm ${USER_HOME}/livecdtmp/extract-cd/casper/filesystem.squashfs

echo "Rebuilding squashFS"
cd ${USER_HOME}/livecdtmp
mksquashfs edit extract-cd/casper/filesystem.squashfs
nano extract-cd/README.diskdefines

echo "Rebuilding md5"
cd extract-cd
rm md5sum.txt
find -type f -print0 | sudo xargs -0 md5sum | grep -v isolinux/boot.cat | sudo tee md5sum.txt

echo "Generating ISO file"
cd ${USER_HOME}/livecdtmp/
mkisofs -r -V "REMASTER" -cache-inodes -J -l -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot -boot-load-size 4 -boot-info-table -o ./REMASTER.iso extract-cd
chmod 777 ./REMASTER.iso

echo "Building complete"

